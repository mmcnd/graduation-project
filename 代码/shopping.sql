/*
Navicat MySQL Data Transfer

Source Server         : ylj
Source Server Version : 80017
Source Host           : localhost:3306
Source Database       : shopping

Target Server Type    : MYSQL
Target Server Version : 80017
File Encoding         : 65001

Date: 2022-06-06 23:32:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `category_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '类型id',
  `name` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '类型名称',
  `product_unit` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '类型产品单位',
  `nav_status` int(1) DEFAULT NULL COMMENT '导航状态',
  `show_status` int(1) DEFAULT NULL COMMENT '展示状态',
  `description` varchar(512) COLLATE utf8_bin DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES ('1527933637524283394', '化肥', '斤', '0', '1', '化肥 主要作用与农作物的生长');
INSERT INTO `category` VALUES ('1527938007556947970', '测试数据（不可选）', '测试数据', '1', '0', '当前分类为测试数据，除了管理员后台其他皆不可选不可见');
INSERT INTO `category` VALUES ('1527938208287948801', '测试数据（导航展示）', '测试数据', '1', '1', '当前为测试数据，导航栏中能展示');
INSERT INTO `category` VALUES ('1527938441721937921', '测试（导航不展示）', '测试数据', '0', '1', '测试数据，当行栏不展示');

-- ----------------------------
-- Table structure for image
-- ----------------------------
DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `image_id` varchar(32) COLLATE utf8_bin NOT NULL,
  `user_id` varchar(32) COLLATE utf8_bin NOT NULL,
  `name` varchar(128) COLLATE utf8_bin DEFAULT NULL,
  `url` varchar(1024) COLLATE utf8_bin NOT NULL,
  `path` varchar(512) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of image
-- ----------------------------
INSERT INTO `image` VALUES ('1527933441365073921', '1527933165660889090', 'file', '2022_05_21\\png\\1527933441365073921.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527933441365073921.png');
INSERT INTO `image` VALUES ('1527937132163760130', '1527936312756142082', '化肥1', '2022_05_21\\png\\1527937132163760130.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527937132163760130.png');
INSERT INTO `image` VALUES ('1527939054622998529', '1527936312756142082', '化肥1', '2022_05_21\\png\\1527939054622998529.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527939054622998529.png');
INSERT INTO `image` VALUES ('1527939261943250945', '1527936312756142082', '化肥1', '2022_05_21\\png\\1527939261943250945.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527939261943250945.png');
INSERT INTO `image` VALUES ('1527939605871984641', '1527936312756142082', '化肥1', '2022_05_21\\png\\1527939605871984641.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527939605871984641.png');
INSERT INTO `image` VALUES ('1527939935271649282', '1527936312756142082', '化肥1', '2022_05_21\\png\\1527939935271649282.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527939935271649282.png');
INSERT INTO `image` VALUES ('1527940087550050306', '1527936312756142082', '化肥1', '2022_05_21\\png\\1527940087550050306.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527940087550050306.png');
INSERT INTO `image` VALUES ('1527940534239232001', '1527936312756142082', 'file', '2022_05_21\\png\\1527940534239232001.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527940534239232001.png');
INSERT INTO `image` VALUES ('1527940755664928770', '1527940615487094786', 'file', '2022_05_21\\png\\1527940755664928770.png', 'G:\\\\Alex\\\\code\\Java\\\\毕业设计\\\\Springboot\\\\images\\2022_05_21\\png\\1527940755664928770.png');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `notice_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公告id',
  `title` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '标题',
  `content` text COLLATE utf8_bin NOT NULL COMMENT '内容',
  `create_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('1527944737561006082', '版本0.1上线公告', 0x23232031E38081E5AE8CE68890E59FBAE69CACE58A9FE883BDE99C80E6B182, '2022-05-21 17:29:48', 'admin');
INSERT INTO `notice` VALUES ('1527945130705702913', '版本0.2上线公告', 0x23232031E38081E6B7BBE58AA0E4BA86E59586E59381E7AEA1E790860A23232032E38081E4BFAEE5A48D627567, '2022-05-21 17:31:22', 'admin');
INSERT INTO `notice` VALUES ('1527945240357392385', '版本0.3上线公告', 0x23232031E38081E4BFAEE5A48D6275670A23232032E38081E6B7BBE58AA0E5AF8CE69687E69CACE5A484E790860A23232033E38081E6B7BBE58AA0E59BBEE78987E5A484E79086, '2022-05-21 17:31:48', 'admin');
INSERT INTO `notice` VALUES ('1527945413271769089', '版本0.4上线公告', 0x23232031E38081E5AF86E7A081E58AA0E5AF860A23232032E38081E5AF86E7A081E6A186E8BE93E585A5E99A90E8978F0A23232033E38081E6B7BBE58AA0E4BFAEE694B9E5AF86E7A081E58A9FE883BD, '2022-05-21 17:32:29', 'admin');
INSERT INTO `notice` VALUES ('1527945553873227778', '版本0.5上线公告', 0x23232031E38081E4BFAEE5A48D6275670A23232032E38081E6B7BBE58AA0E4B9B0E5AEB6E8A792E889B20A23232033E38081E5AE8CE68890E8AEA2E58D95E7AEA1E79086, '2022-05-21 17:33:03', 'admin');
INSERT INTO `notice` VALUES ('1527945673742241794', '版本0.6上线公告', 0x23232031E38081E4BFAEE5A48DE8AEA2E58D956275670A23232032E38081E5AE8CE68890E794A8E688B7E7AEA1E79086, '2022-05-21 17:33:31', 'admin');
INSERT INTO `notice` VALUES ('1527945809054683137', '版本0.7上线公告', 0x23232031E38081E4BFAEE5A48DE4B88AE78988E69CACE98197E6BC8F6275670A23232032E38081E5AE8CE68890E794A8E688B7E9878DE7BDAEE5AF86E7A081, '2022-05-21 17:34:03', 'admin');
INSERT INTO `notice` VALUES ('1527945950251732994', '版本0.8上线公告', 0x23232031E38081E5AE8CE68890E794A8E688B7E4BFA1E681AFE4BFAEE694B90A23232032E38081E6B7BBE58AA0E59586E59381E7B4A2E5BC95, '2022-05-21 17:34:37', 'admin');
INSERT INTO `notice` VALUES ('1527946119038914562', '版本0.9上线公告', 0x23232031E38081E5AE8CE68890E789A9E6B581E7B3BBE7BB9FE5A484E790860A23232032E38081E5AE8CE59684E4B88AE78988E69CACE69CAAE5A484E79086E58A9FE883BD, '2022-05-21 17:35:17', 'admin');
INSERT INTO `notice` VALUES ('1527946280968409089', '版本1.0上线公告', 0x23232031E38081E5AE8CE68890E5A4B4E5838FE98089E68BA90A23232032E38081E4BC98E58C965549E7958CE99DA2, '2022-05-21 17:35:56', 'admin');
INSERT INTO `notice` VALUES ('1527946394424332290', '测试版本发布', 0x23232031E38081E59FBAE4BA8E312E30E5AE8CE68890E6B58BE8AF95, '2022-05-21 17:36:23', 'admin');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `description` varchar(512) COLLATE utf8_bin DEFAULT NULL,
  `category_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `sold` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `image_url` varchar(1024) COLLATE utf8_bin DEFAULT NULL,
  `user_id` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1527937371981479938', '硫酸钾肥农用果树叶面肥高钾肥通用型水溶肥俄罗斯硫酸钾100斤装', '  硫酸钾是无色结晶体,吸湿性小，不宜结块，物理性状良好施用方便，是很好的水溶性钾肥。硫酸钾也是化学中性，生理酸性肥料。\n  硫酸钾是一种无氯,优质高效钾肥,特别是在烟草葡萄，甜菜,茶树，马铃薯,亚麻及各种果树等对氯敏感作物的种植业中是不可缺少的重要肥料，它也是优质氮,磷,钾三元复合肥的主要原料。\n', '1527933637524283394', '3', '97', '2', '140.00', 'http://localhost:8090/image/1527937132163760130', '1527936312756142082');
INSERT INTO `product` VALUES ('1527939147963039746', '测试数据（未上架）', '测试数据，只在商家后台，不给予买家展示', '1527938208287948801', '0', '0', '0', '42.99', 'http://localhost:8090/image/1527939054622998529', '1527936312756142082');
INSERT INTO `product` VALUES ('1527939336291483650', '测试数据（库存不足）', '测试数据，已上架，但是库存不足', '1527938208287948801', '0', '0', '2', '32.30', 'http://localhost:8090/image/1527939261943250945', '1527936312756142082');
INSERT INTO `product` VALUES ('1527939699165888513', '测试数据（拒绝上架）', '测试数据，因价格不合理，管理员拒绝上架', '1527938441721937921', '0', '0', '3', '9999.00', 'http://localhost:8090/image/1527939605871984641', '1527936312756142082');
INSERT INTO `product` VALUES ('1527939980809207809', '测试数据（正常）', '测试数据，正常上架', '1527938208287948801', '2', '98', '2', '40.22', 'http://localhost:8090/image/1527939935271649282', '1527936312756142082');
INSERT INTO `product` VALUES ('1527940111428222977', '测试数据（申请上架）', '测试数据，申请上架', '1527938441721937921', '0', '0', '1', '20.00', 'http://localhost:8090/image/1527940087550050306', '1527936312756142082');

-- ----------------------------
-- Table structure for sys_order
-- ----------------------------
DROP TABLE IF EXISTS `sys_order`;
CREATE TABLE `sys_order` (
  `order_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `cost` decimal(20,2) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `buyer_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `transport_num` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT '',
  `product_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `business_id` varchar(32) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of sys_order
-- ----------------------------
INSERT INTO `sys_order` VALUES ('1527954487854026753', '80.00', '2', '1527940615487094786', 'test', '1527939980809207809', '2', '2022-05-21 18:08:33', '1527936312756142082');
INSERT INTO `sys_order` VALUES ('1527969392220999681', '140.00', '3', '1527940615487094786', '', '1527937371981479938', '1', '2022-05-21 19:07:46', '1527936312756142082');
INSERT INTO `sys_order` VALUES ('1529832316925497345', '140.00', '2', '1527940615487094786', 'test', '1527937371981479938', '1', '2022-05-26 22:30:22', '1527936312756142082');
INSERT INTO `sys_order` VALUES ('1530051334152126466', '280.00', '2', '1527940615487094786', 'test', '1527937371981479938', '2', '2022-05-27 13:00:40', '1527936312756142082');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'userId',
  `user_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户名',
  `identity_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '身份证号',
  `mobile` varchar(11) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '手机号',
  `status` int(1) NOT NULL COMMENT '状态',
  `balance` decimal(64,2) NOT NULL DEFAULT '0.00' COMMENT '余额',
  `password` varchar(64) COLLATE utf8_bin NOT NULL COMMENT '用户密码',
  `roles` int(1) NOT NULL COMMENT '角色',
  `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '电子邮箱',
  `address` varchar(128) COLLATE utf8_bin DEFAULT NULL COMMENT '收货地址',
  `create_time` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `image_url` varchar(256) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1527933165660889090', 'admin', null, null, '0', '0.00', '8f74c3ae2d8263e52bafef4d285c96ac', '0', 'admin@qq.com', null, '2022-05-21 16:43:49', 'http://localhost:8090/image/1527933441365073921');
INSERT INTO `user` VALUES ('1527936312756142082', 'business', null, null, '0', '500.00', 'a602bc65d8be2126b294e216645ca407', '1', 'business@qq.com', null, '2022-05-21 16:56:19', 'http://localhost:8090/image/1527940534239232001');
INSERT INTO `user` VALUES ('1527940615487094786', 'buyer', null, null, '0', '732.20', '60850aa492a398962aced9407d8fc3dd', '2', 'buyer@qq.com', '测试地址：test', '2022-05-21 17:13:25', 'http://localhost:8090/image/1527940755664928770');
INSERT INTO `user` VALUES ('1530016375853088769', 'business1', null, null, '0', '0.00', '0d0e716b014a4f36c7c5c88048e9a210', '1', 'business1@qq.com', null, '2022-05-27 10:41:45', null);
